import React, { Component } from 'react';
import axiosInstance from '../axios-order';


import Modal from '../components/UI/Modal/Modal';
import Aux from '../hoc/Wrapper';

const withErrorHanlder = (WrapperComponent, axios) => {

    return class extends Component {

        state = {
            error: null
        }

        componentWillMount() {

           this.reqInterceptor =  axios.interceptors.request.use(
                req => {
                    this.setState({
                        error: null
                    });
                    return req;
                }
            );

            this.responseInterceptor =  axios.interceptors.response.use(
                res => res,
                error => {
                    this.setState({
                        error: error
                    })
                }
            )
        }

        componentWillUnmount() {
            console.log("unmount called");
            axios.interceptors.request.eject(this.reqInterceptor);
            axios.interceptors.response.eject(this.responseInterceptor);
        }

        errorConfirmHandler= () => {
            this.setState({error: null})
        }

        render() {
            return(
                <Aux>
                    <Modal show={this.state.error} modalClick={this.errorConfirmHandler}>
                        {this.state.error ? this.state.error.message : null}
                    </Modal>
                    <WrapperComponent {...this.props} />
                </Aux>
            )
        }
    }
}

export default withErrorHanlder;