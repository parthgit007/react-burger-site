import React from 'react';
import classes from "./Logo.css";
import burgerLogo from "../../assets/Images/logo.png";

const logo = (props) => (
    <div className={classes.Logo}>
        <img src={burgerLogo} alt=""></img>
    </div>
);

export default logo;