import React, { Component } from "react";
import Aux from "../../hoc/Wrapper";
import classes from "./Layout.css";
import Toolbar from "../Navigation/Toolbar/Toolbar";
import SideDraw from '../Navigation/SideDraw/Sidedraw';

class Layout extends Component {
    state = {
        showSideDraw: false,
    }

    sideDrawCloseHandler = () => {
        this.setState({ showSideDraw: false })
    }

    sideDrawToogleHandler = () => {
        this.setState((prevState) => {
            return { showSideDraw: !prevState.showSideDraw }
        })
    }

    render() {
        return (
            <Aux>
                <Toolbar drawerToggleClicked={this.sideDrawToogleHandler} />
                <SideDraw closed={this.sideDrawCloseHandler} open={this.state.showSideDraw} />
                <main className={classes.Content}>{this.props.children}</main>
            </Aux>
        )
    }
}

export default Layout;
