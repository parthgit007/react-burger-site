import React from 'react';
import Classes from './Spinner.css';

const Spinner = () => {
    return(
        <div className={Classes.Loading}>Loading</div>
    )
}

export default Spinner;