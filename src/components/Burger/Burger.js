import React from "react";
import classes from "./Burger.css";
import BurgerIngredient from "./BurgerIngredient/BurgerIngredient";

// class Burger extends Component {
//   render() {
//     return <div></div>;
//   }
// }

const burger = (props) => {
    // console.log("Burger props", props);
    // console.log("Object Keys", Object.keys(props.ingredient));
    let transformIngredient = Object.keys(props.ingredient)
        .map((igKey) => {
            // console.log("igKey value", igKey);
            // console.log(
            //   "Array(props.ingredient[igKey]) value",
            //   Array(props.ingredient[igKey])
            // );
            // console.log("Array(props.ingredient[igKey]) value", [
            //   ...Array(props.ingredient[igKey]),
            // ]);
            return [...Array(props.ingredient[igKey])].map((_, i) => {
                return <BurgerIngredient key={igKey + i} type={igKey} />;
            });
        })
        .reduce((arr, el) => {
            return arr.concat(el);
        }, []);
    if (transformIngredient.length === 0) {
        transformIngredient = <p>Please start adding ingredient</p>;
    }
    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top" />
            {transformIngredient}
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
};

export default burger;
