import React, { Component } from "react";
import Aux from "../../hoc/Wrapper";
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummery from '../../components/Burger/OrderSummery/OrderSummery';
import axiosInstance from '../../axios-order';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHanlder from '../../withErrorHandler/withErrorHandler';

const INGREDIENT_PRICE = {
    salad: 0.5,
    bacon: 1.0,
    cheese: 1.5,
    meat: 2.5,
};

class BurgerBuilder extends Component {
    state = {
        ingredient: {
            salad: 0,
            bacon: 0,
            cheese: 0,
            meat: 0,
        },
        purchaseable: false,
        purchasing: false,
        totalPrice: 4,
        loading: false
    };

    componentDidMount() {
        const config = {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        }
        axiosInstance.get('/ingredients.json', config)
        .then(res => {
            this.setState({ingredient: res.data})
        })  
        .catch(error => {
            console.log(error)
        })
    }

    updatePurchasebleState(ingredients) {
        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);
        this.setState({
            purchaseable: sum > 0
        });
    }

    addIngredientHandler = (type) => {
        const oldCount = this.state.ingredient[type];
        const updateCount = oldCount + 1;
        const updateIngredient = {
            ...this.state.ingredient,
        };
        updateIngredient[type] = updateCount;
        const priceAddition = INGREDIENT_PRICE[type];
        const oldPirce = this.state.totalPrice;
        const newPrice = oldPirce + priceAddition;
        this.setState({
            ingredient: updateIngredient,
            totalPrice: newPrice,
        });
        this.updatePurchasebleState(updateIngredient);
    };

    removeIngredientHandler = (type) => {
        const oldCount = this.state.ingredient[type];
        if (oldCount <= 0) {
            return;
        }
        const updateCount = oldCount - 1;
        const updateIngredient = {
            ...this.state.ingredient,
        };
        updateIngredient[type] = updateCount;
        const priceDecution = INGREDIENT_PRICE[type];
        const oldPirce = this.state.totalPrice;
        const newPrice = oldPirce - priceDecution;
        this.setState({
            ingredient: updateIngredient,
            totalPrice: newPrice,
        });
        this.updatePurchasebleState(updateIngredient);
    };

    purchaseHandler = () => {
        console.log("here is iti")
        this.setState({
            purchasing: true
        })
    }

    purchaseCancelHandler = () => {
        this.setState({
            purchasing: false
        })
    }

    purchaseContinueHandler = () => {
       // alert('You can continue');
       this.setState({
         purchasing: false,
         loading: true
       })
       const orderObject = {
           ingredient: this.state.ingredient,
           price : this.state.totalPrice,
           customer:{
               name : 'Parth Nayak',
               address : {
                   stree: 'IOC Road',
                   zipCode: 325484,
                   country : 'UK'
               },
               email: 'testerparth@gmail.com'
           },
           deliveryMethod: 'faster'
       };

       axiosInstance.post('/ordersjson', orderObject)
       .then(res => {
           console.log('Response', res);
           this.setState({
               loading: false,
               purchasing: false
           });
       })
       .catch(e => {
           console.log("Error ", e);
           this.setState({
                loading: false,
                purchasing: false
            });
       });
    }

    render() {
        const disableInfo = {
            ...this.state.ingredient,
        };
        for (let key in disableInfo) {
            disableInfo[key] = disableInfo[key] <= 0;
        }

        let burger = (
            <React.Fragment>
                  <Burger ingredient={this.state.ingredient} />
                <BuildControls
                    ingredientAdded={this.addIngredientHandler}
                    ingredientRemove={this.removeIngredientHandler}
                    disabled={disableInfo}
                    price={this.state.totalPrice}
                    purchaseable={this.state.purchaseable}
                    ordered={this.purchaseHandler}
                />
            </React.Fragment>
        )
        return (
            <Aux>
                <Modal show={this.state.purchasing} modalClick={this.purchaseCancelHandler}>
                    {this.state.loading === false && this.state.ingredient ? <OrderSummery
                        ingredients={this.state.ingredient}
                        purchaseCancelled={this.purchaseCancelHandler}
                        purchaseContinue={this.purchaseContinueHandler}
                        price={this.state.totalPrice}
                    />
                    : <Spinner/> }
                </Modal>
                {this.state.ingredient ? burger : <Spinner/>}
            </Aux>
        );
    }
}

export default withErrorHanlder(BurgerBuilder, axiosInstance);
